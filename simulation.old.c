#include "simulation.h"
#include "skfmc.h"

struct Simulation {
	int *values;
	double sum_energies;
	double sum_sqenergies;
	double sum_particles;
};

Simulation *simulation_new() {
	Simulation *self = malloc(sizeof(Simulation));
	self->values = malloc(D*N * sizeof(int));
	self->sum_energies = 0;
	self->sum_sqenergies = 0;
	self->sum_particles = 0;
	return self;
}

void simulation_free(Simulation *self) {
	if (self) {
		free(self->values);
		free(self);
	}
}

void simulation_reset(Simulation *self) {
	int i;
	for (i = 0; i < D*N; ++i) {
		self->values[i] = 0;
	}
}

void simulation_mc_step(Simulation *self, double T) {
	int i, e, n, new;
	for (i = 0; i < ssimcs; ++i) {
		e = gsl_rng_uniform_int(randg, 2) * 2 - 1;
		if (gsl_rng_uniform(randg) < prob(e, T)) {
			n = gsl_rng_uniform_int(randg, D*N);
			new = self->values[n] + e;
			if (new > -1) {
				self->values[n] = new;
			}
		}
	}
}

//void simulation_mc_step(Simulation *self, double T) {
//	int i, e, n, new;
//	double p = prob(-1, T) / (prob(-1, T) + prob(1, T));
//	for (i = 0; i < ssimcs; ++i) {
//		e = (gsl_rng_uniform(randg) < p) ? -1 : 1;
//		n = gsl_rng_uniform_int(randg, D*N);
//		new = self->values[n] + e;
//		if (new > -1) {
//			self->values[n] = new;
//		}
//	}
//}

void simulation_measure(Simulation *self) {
	int i, energy = 0, sqenergy = 0, particles = 0;

	for (i = 0; i < N*D; ++i) {
		energy += self->values[i];
		sqenergy += self->values[i] * self->values[i];
	}

	for (i = 0; i < N; ++i) {
		if ((self->values[D*i] + self->values[D*i+1] + self->values[D*i+2]) == 0) {
			particles += 1;
		}
	}

	self->sum_energies += energy;
	self->sum_sqenergies += sqenergy;
	self->sum_particles += particles;
}

void simulation_print(Simulation *self, Simulation *pprev, Simulation *prev,
                      Simulation *next, Simulation *nnext, double T, int measures) {
	double mean_energy = self->sum_energies / (N * measures);
	double mean_sqenergy = self->sum_sqenergies / (N * measures);
	double mean_energy_sq = mean_energy * mean_energy;

	printf("%f\t%f\t%f\t%f\t%f\t%f\n", T, mean_energy,
		self->sum_particles / measures,
		(next->sum_energies - prev->sum_energies) / (2 * dt * N * measures),
		(pprev->sum_energies - 8 * prev->sum_energies
		    + 8 * next->sum_energies - nnext->sum_energies) / (12 * dt * N * measures),
		(mean_sqenergy - mean_energy_sq) / (N * T * T)
	);
}
