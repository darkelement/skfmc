#include <time.h> // time
#include <stdlib.h> // atoi atof
#include <unistd.h> // getopt
#include <gsl/gsl_rng.h> // Random number generator

#include "simulation.h"
#include "skfmc.h"

double prob(int e, double T) {
	if (e == -1) {
		return 0.5;
	} else {
		return 0.5 * exp(-e / T) / cosh(1.0 / T);
	}
}

int main(int argc, char *argv[]) {
	char c;
	double T;
	int i, s, t, measures, t_measures;
	Simulation **simulation;

	D = 3;
	N = 100;
	ss = 100;
	es = 500;
	ms = 500;
	t_start = 0.1;
	t_stop = 2.0;
	dt = 0.05;

	// ARGUMENT PARSING
	opterr = 0;
	while ((c = getopt(argc, argv, "N:s:e:m:t:T:d:")) != -1) {
		switch (c) {
			case 'N': N = atoi(optarg); break;
			case 's': ss = atoi(optarg); break;
			case 'e': es = atoi(optarg); break;
			case 'm': ms = atoi(optarg); break;
			case 't': t_start = atof(optarg); break;
			case 'T': t_stop = atof(optarg); break;
			case 'd': dt = atof(optarg); break;
			case '?': default:
				printf("Unknown option '-%c'\n", optopt);
				return ERROR_UNKNOWN_OPTION;
		}
	}

	// PRINT PARAMETERS
	printf("# Number of dimensions = %d\n"
	       "# Number of particles = %d\n"
	       "# Simulation steps in Monte Carlo step = %d\n"
	       "# Number of simulations to calculate mean values = %d\n"
	       "# Number of Monte Carlo steps to reach equilibrium = %d\n"
	       "# Number of Monte Carlo steps to calculate mean values = %d\n"
	       "# Start value of temperature = %f\n"
	       "# End value of temperature = %f\n"
	       "# Temperature step = %f\n\n\n",
	       D, N, ssimcs, ss, es, ms, t_start, t_stop, dt);
	fflush(stdout);

	// CHECK INPUT VALUES
	if (t_stop < t_start) {
		printf("Wrong parameters: t_stop < t_start\n");
		return ERROR_WRONG_OPTIONS;
	}
	if (t_start < 2*dt) {
		printf("Wrong parameters: t_start < 2*dt\n");
		return ERROR_WRONG_OPTIONS;
	}
	if (dt <= 0.0) {
		printf("Wrong parameters: dt <= 0.0\n");
		return ERROR_WRONG_OPTIONS;
	}

	// ALLOCATE MEMORY
	randg = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(randg, time(NULL));

	t_start -= 2 * dt;
	t_stop  += 2 * dt;
	t_measures = (int) ((t_stop - t_start) / dt) + 2;

	measures = 0;
	ssimcs = D*N;

	simulation = malloc(t_measures * sizeof(Simulation *));
	for (t = 0; t < t_measures; ++t) {
		simulation[t] = simulation_new();
	}

	// SIMULATION
	for (s = 0; s < ss; ++s) {
		printf("# %d\n", s+1);
		fflush(stdout);

		// RESET PARTICLES
		for (t = 0; t < t_measures; ++t) {
			simulation_reset(simulation[t]);
		}

		// SIMULATION
		for (t = 0; t < t_measures; ++t) {
			T = t * dt + t_start;
			for (i = 0; i < es; ++i) {
				simulation_mc_step(simulation[t], T);
			}
			for (i = 0; i < ms; ++i) {
				simulation_mc_step(simulation[t], T);
				simulation_measure(simulation[t]);
			}
		}
		measures += ms;

		// PRINT RESULTS
		for (t = 2; t < t_measures-2; ++t) {
			simulation_print(simulation[t], simulation[t-2], simulation[t-1],
			          simulation[t+1], simulation[t+2], t * dt + t_start, N * measures);
		}
		printf("\n\n");
		fflush(stdout);
	}
	printf("\n\n# EOS\n");

	// FREE MEMORY
	for (t = 0; t < t_measures; ++t) {
		simulation_free(simulation[t]);
	}
	gsl_rng_free(randg);
	return SUCCESS;
}
