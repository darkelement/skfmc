#include "simulation.h"
#include "skfmc.h"

#include <glib.h>

int VACUUM[3] = {0, 0, 0};

typedef struct {
	int n_particles;
	int d[3];
} State;

State *state_new(int n_particles, int d[3]) {
	State *self = malloc(sizeof(State));
	self->n_particles = n_particles;
	self->d[0] = d[0];
	self->d[1] = d[1];
	self->d[2] = d[2];
	return self;
}

void state_free(State *self) {
	if (self) {
		free(self);
	}
}

struct Simulation {
	GHashTable *states;
	GList *list_states;
	int n_states;

	double sum_energies;
	double sum_sqenergies;
	double sum_particles;
};

State *simulation_get_state(Simulation *self, int d[3]) {
	char *str = g_strdup_printf("%i%i%i", d[0], d[1], d[2]);
	State *state = g_hash_table_lookup(self->states, str);
	g_free(str);
	return state;
}

void simulation_add_state(Simulation *self, State *state) {
	g_hash_table_replace(self->states, g_strdup_printf("%i%i%i", state->d[0], state->d[1], state->d[2]), state);
}

Simulation *simulation_new() {
	Simulation *self = malloc(sizeof(Simulation));
	simulation_reset(self);

	self->sum_energies = 0;
	self->sum_sqenergies = 0;
	self->sum_particles = 0;
	return self;
}

void simulation_free(Simulation *self) {
	if (self) {
		g_hash_table_destroy(self->states);
		g_list_free(self->list_states);
		free(self);
	}
}

void simulation_reset(Simulation *self) {
	State *state = state_new(N, VACUUM);
	self->states = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, (GDestroyNotify) state_free);
	simulation_add_state(self, state);
	self->list_states = g_list_append(NULL, state);
	self->n_states = 1;
}

void simulation_mc_step(Simulation *self, double T) {
	State *state, *newstate;
	int newpos[3], i, d, e;
	double p[2];
	p[0] = prob(-1, T); p[1] = prob(1, T);

	for (i = 0; i < ssimcs; ++i) {
		e = gsl_rng_uniform_int(randg, 2);
		if (gsl_rng_uniform(randg) > p[e]) {
			continue;
		}

		// get state
		state = g_list_nth_data(self->list_states, gsl_rng_uniform_int(randg, self->n_states));

		// get new state
		newpos[0] = state->d[0];
		newpos[1] = state->d[1];
		newpos[2] = state->d[2];
		d = gsl_rng_uniform_int(randg, 3);
		newpos[d] += 2*e-1;
		if (newpos[d] < 0) {
			continue;
		}
		newstate = simulation_get_state(self, newpos);
		if (newstate == NULL) {
			newstate = state_new(0, newpos);
			simulation_add_state(self, newstate);
		}

		// swap particles
		state->n_particles -= 1;
		newstate->n_particles += 1;

		if (state->n_particles == 0) {
			self->list_states = g_list_remove(self->list_states, state);
			self->n_states -= 1;
		}
		if (newstate->n_particles == 1) {
			self->list_states = g_list_append(self->list_states, newstate);
			self->n_states += 1;
		}
	}
}

void simulation_measure(Simulation *self) {
	int energy, sum_energies = 0, sum_sqenergies = 0, particles = 0;
	GList *iter;
	State *state;

	for (iter = self->list_states; iter; iter = iter->next) {
		state = iter->data;
		energy = state->n_particles * (state->d[0] + state->d[1] + state->d[2]);
		sum_energies += energy;
		sum_sqenergies += energy*energy;
	}

	particles = simulation_get_state(self, VACUUM)->n_particles;

	self->sum_energies += sum_energies;
	self->sum_sqenergies += sum_sqenergies;
	self->sum_particles += particles;
}

void simulation_print(Simulation *self, Simulation *pprev, Simulation *prev,
                      Simulation *next, Simulation *nnext, double T, int measures) {
	double mean_energy = self->sum_energies / (N * measures);
	double mean_sqenergy = self->sum_sqenergies / (N * measures);
	double mean_energy_sq = mean_energy * mean_energy;

	printf("%f\t%f\t%f\t%f\t%f\t%f\n", T, mean_energy,
		self->sum_particles / measures,
		(next->sum_energies - prev->sum_energies) / (2 * dt * N * measures),
		(pprev->sum_energies - 8 * prev->sum_energies
		    + 8 * next->sum_energies - nnext->sum_energies) / (12 * dt * N * measures),
		(mean_sqenergy - mean_energy_sq) / (T * T)
	);
}
