#ifndef __SKFMC_H__
#define __SKFMC_H__

#include <math.h> // tanh
#include <gsl/gsl_rng.h>

#define SUCCESS              0
#define ERROR_UNKNOWN_OPTION 1
#define ERROR_WRONG_OPTIONS  2

int D;          // Dimensions
int N;          // Number of bosons
int ssimcs;     // Simulation steps in Monte Carlo step (should be equal D*N)
int ss;         // Number of simulations to calculate mean values
int es;         // Number of Monte Carlo steps to reach equilibrium
int ms;         // Number of Monte Carlo steps to calculate mean values
double t_start; // Start value of temperature
double t_stop;  // End value of temperature
double dt;      // Temperature step

gsl_rng *randg; // Random number generator

double prob(int e, double T);

#endif // __SKFMC_H__
