#include <math.h>
#include <time.h>
#include <stdio.h>
#include <sys/time.h>
#include <gsl/gsl_rng.h>

double get_miliseconds(struct timeval tv_start, struct timeval tv_stop) {
	return 1000 * (tv_stop.tv_sec  - tv_start.tv_sec)
	    + 0.001 * (tv_stop.tv_usec - tv_start.tv_usec);
}

double test_exponents(double x) {
	return 0.5 * exp(-x) / cosh(x);
}

double test_tangent(double x) {
	return 0.5 * tanh(-x) + 0.5;
}

void test_random(gsl_rng *rand) {
	gsl_rng_uniform_int(rand, 600);
	gsl_rng_uniform_int(rand, 3);
	gsl_rng_uniform_int(rand, 2);
}

void test_division(gsl_rng *rand) {
	int r, n, p, e;
	r = gsl_rng_uniform_int(rand, 1800);
	n = r / 6;
	p = r % 3;
	e = r % 2;
	r = n + p + e; // low cost operation just to avoid warnings
}

int main() {
	int i, j, times = 10000000;
    struct timeval tv_start, tv_stop;

	double (*funcs1[2])(double);
	void (*funcs2[2])(gsl_rng *);
	double diff[2];

	// exponents vs tangent

	funcs1[0] = test_exponents;
	funcs1[1] = test_tangent;

	for (j = 0; j < 2; ++j) {
		gettimeofday(&tv_start, NULL);
		for(i = 0; i < times; i++) {
			funcs1[j](i / times);
		}
		gettimeofday(&tv_stop, NULL);
		diff[j] = get_miliseconds(tv_start, tv_stop);
	}

    printf("\nElapsed time [microseconds]:\n\texponents:\t%f\n"
	       "\ttangent:\t%f\nIt's better to use %s\n\n",
	       diff[0], diff[1], diff[0] < diff[1] ? "exponents" : "tangent");

	// random generator vs division with rest

	gsl_rng *rand = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(rand, time(NULL));

	funcs2[0] = test_random;
	funcs2[1] = test_division;

	for (j = 0; j < 2; ++j) {
		gettimeofday(&tv_start, NULL);
		for(i = 0; i < times; i++) {
			funcs2[j](rand);
		}
		gettimeofday(&tv_stop, NULL);
		diff[j] = get_miliseconds(tv_start, tv_stop);
	}

    printf("\nElapsed time [microseconds]:\n\trandom:\t\t%f\n"
	       "\tdivision:\t%f\nIt's better to use %s\n\n",
	       diff[0], diff[1], diff[0] < diff[1] ? "random" : "division");

	gsl_rng_free(rand);

    return 0;
}
