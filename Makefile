CC = gcc -Wall ${debug_flags}
PC = pkg-config --libs --cflags

math_flags = -lm
gsl_flags  = `${PC} gsl`
glib_flags = `${PC} glib-2.0`

ifdef TYPE
	skfmc_deps = skfmc.c simulation.${TYPE}.c
else
	skfmc_deps = skfmc.c simulation.c
endif
skfmc_headers = skfmc.h simulation.h
skfmc_test_deps = skfmc_test.c

all: skfmc

test: skfmc_test
	./skfmc_test

clear:
	rm -f skfmc skfmc_test *.eps *.dat *.aux *.pdf *.log results/*.pdf

skfmc: ${skfmc_deps} ${skfmc_headers} Makefile
	${CC} ${skfmc_deps} ${math_flags} ${gsl_flags} ${glib_flags} -o skfmc

skfmc_test: ${skfmc_test_deps} Makefile
	${CC} ${skfmc_test_deps} ${math_flags} ${gsl_flags} -o skfmc_test

report.pdf: report.tex Makefile
	pdflatex report.tex
