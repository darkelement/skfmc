#!/bin/bash

index=98
dir=.

while test "$1"; do
	case $1 in
		-d|--dir)
			if test -r "$2/skfmc.dat"; then
				dir="$2";
			else
				echo "Can not read file '$2/skfmc.dat'"
				exit 1
			fi
			shift
		;;
		-i|--index)
			index=$(($2-1));
			shift
		;;
		*)
			echo "Unknown option '$1'"
			exit 1
		;;
	esac
	shift
done

tstart="`cat \"$dir/skfmc.dat\" | grep 'Start value of temperature' | cut -d '=' -f 2`"
tstop="` cat \"$dir/skfmc.dat\" | grep 'End value of temperature'   | cut -d '=' -f 2`"

cd "$dir"
gnuplot <<EOF
set term postscript enhanced
set size 1.0, 1.0

set xlabel "Temperature"

set key right top

set ylabel "Number of particles"
set output "particles.eps"
plot [$tstart:$tstop] "skfmc.dat" index $index using 1:3 title "number of particles" with lines

set key right bottom

set ylabel "Energy"
set output "energy.eps"
plot [$tstart:$tstop] "skfmc.dat" index $index using 1:2 title "energy" with lines

set ylabel "Heat capacity"
set output "heat.eps"
plot [$tstart:$tstop] \
     "skfmc.dat" index $index using 1:4 title "heat capacity [three-point]" with lines, \
     "skfmc.dat" index $index using 1:5 title "heat capacity [five-point]" with lines, \
     "skfmc.dat" index $index using 1:6 title "heat capacity [energy fluctuations]" with lines
EOF
