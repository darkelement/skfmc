#ifndef __SIMULATION_H__
#define __SIMULATION_H__

struct Simulation;
typedef struct Simulation Simulation;

Simulation *simulation_new();
void simulation_free(Simulation *self);

void simulation_reset(Simulation *self);
void simulation_mc_step(Simulation *self, double T);
void simulation_measure(Simulation *self);
void simulation_print(Simulation *self, Simulation *pprev, Simulation *prev,
                      Simulation *next, Simulation *nnext, double T, int measures);

#endif // __SIMULATION_H__
