#include "simulation.h"
#include "skfmc.h"

struct Simulation {
	int *particle;
	double max_energy;
	double sum_energies;
	double sum_sqenergies;
	double sum_particles;
};

Simulation *simulation_new() {
	Simulation *self = malloc(sizeof(Simulation));
	self->particle = malloc(N * sizeof(int));
	self->max_energy = 0;
	self->sum_energies = 0;
	self->sum_sqenergies = 0;
	self->sum_particles = 0;
	return self;
}

void simulation_free(Simulation *self) {
	if (self) {
		free(self->particle);
		free(self);
	}
}

void simulation_reset(Simulation *self) {
	int i;
	for (i = 0; i < N; ++i) {
		self->particle[i] = 0;
	}
}

//void simulation_mc_step(Simulation *self, double T) {
//	int i, j, e, ch_en, new;
//	double p = prob(-1, T) / (prob(-1, T) + prob(1, T));
//	for (i = 0; i < ssimcs; ++i) {
//		e = (gsl_rng_uniform(randg) < p) ? -1 : 1;
//		ch_en = gsl_rng_uniform_int(randg, self->max_energy+1);

//		for (j = 0; j < N; ++j) {
//			if (self->particle[j] <= ch_en) {
//				break;
//			}
//		}

//		new = ch_en + e;
//		if (new > -1) {
//			self->particle[j] = new;
//			if (new > self->max_energy) {
//				self->max_energy = new;
//			}
//		}
//	}
//}

void simulation_mc_step(Simulation *self, double T) {
	int i, j, e, ch_en, new;
	for (i = 0; i < ssimcs; ++i) {
		e = gsl_rng_uniform_int(randg, 2) * 2 - 1;
		if (gsl_rng_uniform(randg) > prob(e, T)) {
			continue;
		}

		ch_en = gsl_rng_uniform_int(randg, self->max_energy+1);

		for (j = 0; j < N; ++j) {
			if (self->particle[j] <= ch_en) {
				break;
			}
		}

		new = ch_en + e;
		if (new > -1) {
			self->particle[j] = new;
			if (new > self->max_energy) {
				self->max_energy = new;
			}
		}
	}
}

void simulation_measure(Simulation *self) {
	int i, energy = 0, sqenergy = 0, particles = 0;

	for (i = 0; i < N; ++i) {
		energy += self->particle[i];
		sqenergy += self->particle[i] * self->particle[i];
	}

	for (i = 0; i < N; ++i) {
		if (self->particle[i] == 0) {
			particles += 1;
		}
	}

	self->sum_energies += energy;
	self->sum_sqenergies += sqenergy;
	self->sum_particles += particles;
}

void simulation_print(Simulation *self, Simulation *pprev, Simulation *prev,
                      Simulation *next, Simulation *nnext, double T, int measures) {
	double mean_energy = self->sum_energies / (N * measures);
	double mean_sqenergy = self->sum_sqenergies / (N * measures);
	double mean_energy_sq = mean_energy * mean_energy;

	printf("%f\t%f\t%f\t%f\t%f\t%f\n", T, mean_energy,
		self->sum_particles / measures,
		(next->sum_energies - prev->sum_energies) / (2 * dt * N * measures),
		(pprev->sum_energies - 8 * prev->sum_energies
		    + 8 * next->sum_energies - nnext->sum_energies) / (12 * dt * N * measures),
		(mean_sqenergy - mean_energy_sq) / (N * T * T)
	);
}
